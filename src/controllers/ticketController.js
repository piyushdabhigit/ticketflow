import Ticket from '../models/Ticket.js';
import { sendEmailNotification } from '../services/notificationService.js';
import multer from 'multer';
import path from 'path';
import mongoose from 'mongoose';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/'); // Specify the folder where uploaded files will be stored
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const fileExtension = path.extname(file.originalname);
    const fileName = 'content-' + uniqueSuffix + fileExtension;
    cb(null, fileName);
  },
});

const upload = multer({ storage });

// Dynamic success response function
const sendSuccess = (res, message = 'Success', data = null, statusCode = 200) => {
  return res.status(statusCode).json({ success: true, message, data });
};

// Dynamic error response function
const sendError = (res, message = 'Internal Server Error', statusCode = 500, error = null) => {
  return res.status(statusCode).json({ success: false, message, error });
};

// Create a new ticket
export const createTicket = async (req, res) => {
  try {
    upload.single('file')(req, res, async (err) => {
      if (err instanceof multer.MulterError) {
        return sendError(res, 'File upload failed', 400, err.message);
      } else if (err) {
        return sendError(res, 'File upload failed', 400, err);
      }

     
      const { title, description, contentType } = req.body;

      // Check if a file was uploaded
      if (!req.file && !contentType == 'text') {
        return sendError(res, 'No file uploaded', 400);
      }

      if (!['text', 'image', 'video'].includes(contentType)) {
        return sendError(res, 'Invalid content type', 400);
      }

      var ticket;

      if (contentType === 'text') {
        // Handle text content
        const textContent = req.body.textContent;
        ticket = new Ticket({ title, description, contentType, content: textContent, user: req.userId });
      
        await ticket.save();
      } else if (contentType === 'image' || contentType === 'video') {
        ticket = new Ticket({ title, description, contentType, content: req.file.filename, user: req.userId });
       
        await ticket.save();
      }
 

      // Respond with success
      return sendSuccess(res, 'Ticket created successfully', ticket);

    });
  } catch (error) {
    console.error('Error creating ticket:', error);
    return sendError(res);
  }
};

// Edit an existing ticket
export const editTicket = async (req, res) => {
  try {
    upload.single('file')(req, res, async (err) => {
      if (err instanceof multer.MulterError) {
        return sendError(res, 'File upload failed', 400, err.message);
      } else if (err) {
        return sendError(res, 'File upload failed', 400, err);
      }

       
      const { title, description, contentType, ticketId } = req.body;

      // Check if a file was uploaded
      if (!req.file && !contentType == 'text') {
        return sendError(res, 'No file uploaded', 400);
      }

      if (!['text', 'image', 'video'].includes(contentType)) {
        return sendError(res, 'Invalid content type', 400);
      }

      // Find the ticket by ID
      const ticket = await Ticket.findById(ticketId);

      if (!ticket) {
        return sendError(res, 'Ticket not found', 404);
      }

      // Update ticket details
      ticket.title = title;
      ticket.description = description;
      ticket.contentType = contentType;

      if (contentType === 'text') {
        
        const textContent = req.body.textContent; // Assuming you have a "textContent" field in the request
        ticket.content = textContent;
      } else if (contentType === 'image' || contentType === 'video') {
        ticket.content = req.file.filename;
      }

      // Save the updated ticket
      await ticket.save();

      // Respond with success
      return sendSuccess(res, 'Ticket updated successfully', { ticket });
    });
  } catch (error) {
    console.error('Error updating ticket:', error);
    return sendError(res);
  }
};

// Add Remarks to Ticket
export const addRemarks = async (req, res) => {
  try {
    const { ticketId, remarks } = req.body;

    const ticket = await Ticket.findById(ticketId);

    if (!ticket) {
      return sendError(res, 'Ticket not found', 404);
    }

     
    const newRemark = {
      text: remarks,
      user: req.userId,
      role: req.userRole,
    };

    ticket.remarks.push(newRemark);

    await ticket.save();

    return sendSuccess(res, 'Remarks added successfully', { ticket });
  } catch (error) {
    console.error(error);
    return sendError(res);
  }
};

// Approve a ticket 
export const approveTicket = async (req, res) => {
  try {
   
    const { ticketId, remarksText } = req.body;

     
    const ticket = await Ticket.findById(ticketId);

    // Check if the ticket exists
    if (!ticket) {
      return sendError(res, 'Ticket not found', 404);
    }

    // Check if the ticket has already been approved
    if (ticket.status === 'approved') {
      return sendError(res, 'Ticket has already been approved.', 400);
    }
 
    ticket.status = 'approved';

    // Add remarks to the ticket
    ticket.remarks.push({
      user: req.userId,
      text: remarksText,
      role: req.userRole,
    });

    await ticket.save();

    // Notify the admin if the ticket is approved by a Manager (admin will get it)| uncomment if you have credentials
    // if (req.userRole === 'manager') {
    //   await sendEmailNotification('admin@gmail.com', 'A manager has approved a ticket.');
    // } 

    // if (req.userRole === 'admin') {
    //   await sendEmailNotification('client@gmail.com', 'admin has approved a ticket.');
    // } 

    return sendSuccess(res, 'Ticket approved by manager', { ticket });
  } catch (error) {
    console.error(error);
    return sendError(res);
  }
};

// Disapprove Ticket
export const disapproveTicket = async (req, res) => {
  try {
    const { ticketId, remarksText } = req.body;

    
    const ticket = await Ticket.findById(ticketId);

    if (!ticket) {
      return sendError(res, 'Ticket not found', 404);
    }

    // Check if the ticket has already been approved
    if (ticket.status === 'disapproved') {
      return sendError(res, 'Ticket has already been disapproved.', 400);
    } 

    ticket.status = 'disapproved';
    // Add remarks to the ticket
    ticket.remarks.push({
      user: req.userId,
      text: remarksText,
      role: req.userRole,
    });

    await ticket.save();

    // Notify the client | uncomment if you have credentials
    //if (req.userRole === 'admin') {
    //    await sendEmailNotification('emailId', `A ${req.userRole} has disapproved a ticket.`);
    //} 

    return sendSuccess(res, 'Ticket disapproved successfully', { ticket });

  } catch (error) {
    console.error(error);
    return sendError(res);
  }
};

// View All Stages of Ticket
export const viewAllStages = async (req, res) => {
  try {
    const { ticketId } = req.query;

    if (ticketId) {
      // Check if ticketId is a valid ObjectId
      if (!mongoose.Types.ObjectId.isValid(ticketId)) {
        return sendError(res, 'Invalid ticketId format', 400);
      }

      // Find a specific ticket by ID
      const ticket = await Ticket.findById(ticketId);

      if (!ticket) {
        return sendError(res, 'Ticket not found', 404);
      }

      return sendSuccess(res, 'Ticket retrieved successfully', { ticket });
    } else {
      // Find all tickets
      const tickets = await Ticket.find();

      return sendSuccess(res, 'Tickets retrieved successfully', { tickets });
    }
  } catch (error) {
    console.error(error);
    return sendError(res);
  }
};
