import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { validationResult } from 'express-validator';
import { jwtSecret } from '../config.js';
import User from '../models/User.js';

// Dynamic
// Send a success response with data
const sendSuccess = (res, data) => {
  return res.status(200).json({ success: true, data });
};

// Send an error response with an optional message and status code
const sendError = (res, message = 'Internal Server Error', statusCode = 500) => {
  return res.status(statusCode).json({ success: false, message });
};

// User registration
export const register = async (req, res) => {
  try {
    const { username, email, password, role } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) return sendError(res, 'Validation failed', 400);

    const existingUser = await User.findOne({ email });
    if (existingUser) return sendError(res, 'User with this email already exists', 400);

    const hashedPassword = await bcrypt.hash(password, 10);

    const user = new User({ username, email, role, password: hashedPassword });

    await user.save();

    const token = jwt.sign({ userId: user._id, role: user.role }, jwtSecret, { expiresIn: '1d' });

    sendSuccess(res, { token, user });
  } catch (error) {
    console.error(error);
    sendError(res);
  }
};
 

// User login
export const login = async (req, res) => {
  try { 
    
    const { email, password } = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) return sendError(res, 'Validation failed', 400);

    const user = await User.findOne({ email });

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return sendError(res, 'Invalid credentials', 401);
    }

    const token = jwt.sign({ userId: user._id, role: user.role }, jwtSecret, { expiresIn: '1d' });

    sendSuccess(res, { token, user });
  } catch (error) {
    console.error(error);
    sendError(res);
  }
};
