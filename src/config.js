export const jwtSecret = process.env.JWT_SECRET || 'your-default-secret-key';
export const databaseURL = process.env.DATABASE_URL || 'mongodb://localhost:9000/ticketflow';
export const port = process.env.PORT || 3000;
