import express from 'express';
import {
  createTicket, 
  addRemarks,
  approveTicket,
  disapproveTicket,
  viewAllStages,
  editTicket
} from '../controllers/ticketController.js';
import { authenticateToken } from '../middlewares/authMiddleware.js';
import { checkUserRole } from '../middlewares/roleMiddleware.js';

const router = express.Router();

//role management 
router.post('/create', authenticateToken, createTicket);
router.post('/edit', authenticateToken, editTicket);
router.post('/addRemarks', authenticateToken, checkUserRole(['employee','manager','admin']), addRemarks);
router.post('/approve', authenticateToken, checkUserRole(['manager','manager','admin']), approveTicket);
router.post('/disapprove', authenticateToken, checkUserRole(['manager','manager','admin']), disapproveTicket);
router.get('/viewAllStages', authenticateToken, checkUserRole(['admin']), viewAllStages);

export default router;
