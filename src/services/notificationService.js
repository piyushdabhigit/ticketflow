import nodemailer from 'nodemailer';
import dotenv from 'dotenv';
 
dotenv.config();
 
const transporter = nodemailer.createTransport({
  service: 'Gmail', // e.g., 'Gmail', 'Outlook', or use SMTP settings
  auth: {
    user: process.env.EMAIL_USER,  
    pass: process.env.EMAIL_PASS,  
  },
});
 
export async function sendEmailNotification(recipientEmail, message) {
  try {
    
    const info = await transporter.sendMail({
      from: process.env.EMAIL_USER,  
      to: recipientEmail,  
      subject: 'Notification from Your App', 
      text: message, 
    });

   
  } catch (error) {
    console.error('Email error:', error);
  }
}


 
// //import NotificationService from 'notification-service';
// export const sendNotification = async (userId, message) => {
//   try {
//     // Initialize the notification service
//     const notification = new NotificationService();

//     // Send a notification to the user with the specified message
//     await notification.send(userId, message);

//     // Log a success message or handle any errors as needed
//     console.log(`Notification sent to user ${userId}: ${message}`);
//   } catch (error) {
//     console.error('Error sending notification:', error);
//   }
// };
