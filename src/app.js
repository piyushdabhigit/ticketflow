import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import authRoutes from './routes/authRoutes.js';
import ticketRoutes from './routes/ticketRoutes.js';
import ejs from 'ejs';
import dotenv from 'dotenv'; // Import dotenv

dotenv.config(); // Load environment variables from a .env file

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.set('views', './views');
app.use(express.static('public'));

mongoose.connect(process.env.DATABASE_URL, { // Use the DATABASE_URL environment variable
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB');
});

console.log('DATABASE_URL:', process.env.DATABASE_URL);


app.get('/', (req, res) => {
  res.send('Hello, NinjaTech!');
});

//Test server
app.get('/login', (req, res) => {
  res.render('login', { process });
});

app.get('/register', (req, res) => {
  res.render('register', { process });
});

// Dashboard route
app.get('/dashboard', (req, res) => { 
  const data = {
    pageTitle: 'Dashboard',
    // Add more data as needed
  }; 
  res.render('dashboard', data);
});


app.use('/api/auth', authRoutes);
app.use('/api/tickets', ticketRoutes);

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Server is running on port ${port}...`);
});
