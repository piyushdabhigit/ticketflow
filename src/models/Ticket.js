import mongoose from 'mongoose';

const ticketSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User', // Reference to the User model
    required: true,
  },
  content: {
    type: String, 
    required: true,
  },
  contentType: {
    type: String,
    enum: ['text', 'image', 'video'], 
    required: true,
  },
  
  remarks: [
    {
      text: String,               
      user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',             
      },
      role: {
        type: String,          
        enum: ['employee', 'manager', 'admin'], 
      },
      createdAt: {
        type: Date,
        default: Date.now,      
      },
    },
  ],
  status: {
    type: String,
    enum: ['pending', 'approved', 'disapproved'],
    default: 'pending',
  },
  createdAt: {
    type: Date,
    default: Date.now,       
  }
},
{
  timestamps: true,  
});

const Ticket = mongoose.model('Ticket', ticketSchema);

export default Ticket;
