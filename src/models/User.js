import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  username: {
    type: String
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['admin', 'manager', 'employee', 'client'],
    default: 'client', // Set the default role
  },
  createdAt: {
    type: Date,
    default: Date.now,     
  }
},
{
  timestamps: true,  
});

const User = mongoose.model('User', userSchema);

export default User;
