import jwt from 'jsonwebtoken';
import { jwtSecret } from '../config.js';

export const authenticateToken = (req, res, next) => {
  const token = req.header('Authorization');

  if (!token) {
    return res.status(401).json({ success: false, message: 'Access denied. No token provided.', data: null });
  }

  try {
    const decoded = jwt.verify(token, jwtSecret);

    req.userId = decoded.userId;
    req.userRole = decoded.role;

    next();
  } catch (error) {
    console.error(error);
    res.status(403).json({ success: false, message: 'Invalid token', data: null });
  }
};
