import jwt from 'jsonwebtoken';
import { jwtSecret } from '../config.js';

export const checkUserRole = (allowedRoles) => {
  return (req, res, next) => {
    const token = req.header('Authorization');

    if (!token) {
      return res.status(401).json({ success: false, message: 'Access denied. No token provided.', data: null });
    }

    try {
      const decoded = jwt.verify(token, jwtSecret);

      if (!allowedRoles.includes(decoded.role)) {
        return res.status(403).json({ success: false, message: `Access denied. You do not have the required role (${decoded.role}).`, data: null });
      }

      req.userId = decoded.userId;
      req.userRole = decoded.role;

      next();
    } catch (error) {
      console.error(error);
      res.status(403).json({ success: false, message: 'Invalid token', data: null });
    }
  };
};
