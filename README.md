# Ticket Management System API Documentation

This API documentation provides details about the endpoints and requests for the Ticket Management System.

## Authentication

Before using the API, you need to register and log in to obtain an authentication token.

### Register User

- **Endpoint:** `/api/auth/register`
- **Method:** POST
- **Request:**
  - Body:
    - `username` (string, required): User's username.
    - `email` (string, required): User's email.
    - `password` (string, required): User's password.
    - `role` (string, required): User's role (e.g., 'manager', 'employee').

### Log In

- **Endpoint:** `/api/auth/login`
- **Method:** POST
- **Request:**
  - Body:
    - `email` (string, required): User's email.
    - `password` (string, required): User's password.
- **Response:**
  - Returns a JWT (JSON Web Token) for authentication.

## Ticket Management

Manage tickets, create, edit, and view details.

### Create Ticket

- **Endpoint:** `/api/tickets/create`
- **Method:** POST
- **Request:**
  - Body:
    - `title` (string, required): Ticket title.
    - `description` (string, required): Ticket description.
    - `contentType` (string, required): Type of content ('text', 'image', 'video').
    - `textContent` (string, optional): Required for 'text' content type.
    - `file` (file, optional): Required for 'image' or 'video' content type.

### Add Remarks to a Ticket

- **Endpoint:** `/api/tickets/addRemarks`
- **Method:** POST
- **Request:**
  - Body:
    - `ticketId` (string, required): ID of the ticket.
    - `remarks` (string, required): Remarks to be added.

### Approve a Ticket

- **Endpoint:** `/api/tickets/approve`
- **Method:** POST
- **Request:**
  - Body:
    - `ticketId` (string, required): ID of the ticket.
    - `remarksText` (string, optional): Remarks for approval.

### Disapprove a Ticket

- **Endpoint:** `/api/tickets/disapprove`
- **Method:** POST
- **Request:**
  - Body:
    - `ticketId` (string, required): ID of the ticket.
    - `remarksText` (string, optional): Remarks for disapproval.

### Edit Ticket

- **Endpoint:** `/api/tickets/edit`
- **Method:** POST
- **Request:**
  - Body:
    - `title` (string, optional): New ticket title.
    - `description` (string, optional): New ticket description.
    - `contentType` (string, optional): New content type ('text', 'image', 'video').
    - `textContent` (string, optional): New text content.
    - `file` (file, optional): New image or video content.
    - `ticketId` (string, required): ID of the ticket to be edited.

### View Ticket Stages

- **Endpoint:** `/api/tickets/viewAllStages`
- **Method:** GET
- **Request:**
  - Query Params:
    - `ticketId` (string, optional): ID of the ticket to view stages.


## DEMO FOR LOGIN AND REGISTER

http://localhost:3000/register
http://localhost:3000/login

## DB Design DEMO
# User Collection:
{
    "_id" : ObjectId("651e9b253fd35ffd8b6f8e79"),
    "username" : "admin",
    "email" : "admin@example1.com",
    "password" : "$2b$10$XPVGeX7x9PL2Q5KsOvZiK.Z55lqe5wGRhtzXSMVbzF29okYwi0IRa",
    "role" : "admin",
    "createdAt" : ISODate("2023-10-05T11:16:53.313Z"),
    "updatedAt" : ISODate("2023-10-05T11:16:53.313Z"),
    "__v" : 0
}

# Ticket collection
{
    "_id" : ObjectId("651e9cf4fa6aadd8819befc2"),
    "title" : "NEW",
    "description" : "cool",
    "user" : ObjectId("651e9c740e8c1ed59a4456af"),
    "content" : "content-1696505076354-315413442.mp4",
    "contentType" : "video",
    "status" : "pending",
    "remarks" : [ 
        {
            "text" : "This is an important remark from the employee.",
            "user" : ObjectId("651e9cbafa6aadd8819befbc"),
            "role" : "admin",
            "_id" : ObjectId("651ea0eefa6aadd8819befca"),
            "createdAt" : ISODate("2023-10-05T11:41:34.010Z")
        }
    ],
    "createdAt" : ISODate("2023-10-05T11:24:36.405Z"),
    "updatedAt" : ISODate("2023-10-05T11:41:34.023Z"),
    "__v" : 1
}

## Contributors

- Piyush Dabhi

 ## NOTE i already add all request in postman collection pls check it 